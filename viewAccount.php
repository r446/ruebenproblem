
<html>
	<head>
		<title>Kraut und Rueben</title>
	</head>
	<body>
		<h1>Kraut und Rueben - Nutzerkonto</h1>
		<form action="" method="post">
			<?php
				$servername = "localhost";
				$username = "root";
				$password = "";
				$dbname = "rubenkraut";

				// Create connection
				$conn = mysqli_connect($servername, $username, $password, $dbname);
				
				// Check connection
				if (!$conn) {
					die("Connection failed: " . mysqli_connect_error());
				}
				
				if (isset($_POST['customerId'])) {
					$customerId = $_POST['customerId'];
					
					//Prüfen ob Kunde existiert
					$queryFindValue = ("
						SELECT KUNDENNR
						FROM KUNDE
						WHERE KUNDENNR = '" . $_POST['customerId'] . "';
					");
					$result = mysqli_query($conn, $queryFindValue);
					if (mysqli_num_rows($result) > 0) {
						//Daten aktualisieren
						if (isset($_POST['cNameLast'])) {
							$queryUpdateCustomerData = ("
								UPDATE KUNDE
								SET NACHNAME = '" . $_POST["cNameLast"] . "',
									VORNAME = '" . $_POST["cNameFirst"] . "',
									GEBURTSDATUM = '" . $_POST["cDOB"] . "',				
									STRASSE = '" . $_POST["cStreet"] . "',
									HAUSNR = '" . $_POST["cStreetNo"] . "',
									PLZ = '" . $_POST["cZIP"] . "',							
									ORT = '" . $_POST["cCity"] . "',							
									TELEFON = '" . $_POST["cPhone"] . "',							
									EMAIL = '" . $_POST["cMail"] . "'
								WHERE KUNDENNR = " . $customerId . ";
							");
							mysqli_query($conn, $queryUpdateCustomerData);
							
						}
						
						//Kundendaten ausgeben
						$querypostCustomerData = ("
							SELECT *
							FROM KUNDE
							WHERE KUNDENNR = " . $customerId . ";
						");
						$result = mysqli_query($conn, $querypostCustomerData);
						if (mysqli_num_rows($result) > 0) {
							$row = mysqli_fetch_assoc($result);
							echo "<h3>Kundennummer: " . $row["KUNDENNR"] . "</h3>";
							echo "Nachname";
							echo "<br><input type=\"text\" name=\"cNameLast\" value=\"" . $row["NACHNAME"] . "\" />";
							echo "<br>Vorname";
							echo "<br><input type=\"text\" name=\"cNameFirst\" value=\"" . $row["VORNAME"] . "\" />";
							echo "<br>Geburtsdatum";
							echo "<br><input type=\"text\" name=\"cDOB\" value=\"" . $row["GEBURTSDATUM"] . "\" />";
							echo "<br>Strasse / Hausnummer";
							echo "<br><input type=\"text\" name=\"cStreet\" value=\"" . $row["STRASSE"] . "\" />";
							echo "<input type=\"text\" name=\"cStreetNo\" value=\"" . $row["HAUSNR"] . "\" />";
							echo "<br>PLZ / Ort";
							echo "<br><input type=\"text\" name=\"cZIP\" value=\"" . $row["PLZ"] . "\" />";
							echo "<input type=\"text\" name=\"cCity\" value=\"" . $row["ORT"] . "\" />";
							echo "<br>Telefon";
							echo "<br><input type=\"text\" name=\"cPhone\" value=\"" . $row["TELEFON"] . "\" />";
							echo "<br>EMail";
							echo "<br><input type=\"text\" name=\"cMail\" value=\"" . $row["EMAIL"] . "\" />";
							
							//echo "<br><br><input type=\"submit\" name=\"updateData\" value=\"Absenden\" />";
							echo "<br><br><button type=\"submit\" name=\"customerId\" value=\"" . $customerId . "\">Absenden</button>";
						}
						
						//Bestellungen ausgeben
						$queryOrders = ("
							SELECT *
							FROM BESTELLUNG
							WHERE KUNDENNR = " . $customerId . ";
						");
						$result = mysqli_query($conn, $queryOrders);
						if (mysqli_num_rows($result) > 0) {
							echo "<br><br>";
							echo "<table>";
							echo "<tr>";
							
							echo "<th>BESTELLNR</th>";
							echo "<th>BESTELLDATUM</th>";
							echo "<th>RECHNUNGSBETRAG</th>";
							echo "</tr>";
							
							while ($row = mysqli_fetch_assoc($result)) {
								echo "<tr>";
								echo "<td>" . $row["BESTELLNR"] . "</td>";
								echo "<td>" . $row["BESTELLDATUM"] . "</td>";
								echo "<td>" . $row["RECHNUNGSBETRAG"] . " EUR</td>";
								echo "</tr>";
							}
							
							echo "</table>";
							echo "<br>";
						}
						mysqli_close($conn);
					}
				}
			?>
		</form>
		<form action="/deleteAccount.php" method="post">
			<button type="submit" name="deleteUser" value="<?php echo $customerId; ?>">Konto loeschen</button>
		</form>
		<form action="/chooseAction.php" method="post">
			<input type="submit" value="Startseite" />
		</form>
	</body>
</html>