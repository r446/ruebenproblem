USE rubenkraut;
DELETE FROM REZEPT;
DELETE FROM REZEPT_ZUTAT;
DELETE FROM REZEPT_KATEGORIE;

INSERT INTO REZEPT (REZEPTNR, BEZEICHNUNG) VALUES (100, 'Wurstsuppe');
INSERT INTO REZEPT (REZEPTNR, BEZEICHNUNG) VALUES (101, 'Tomozzarella');
INSERT INTO REZEPT (REZEPTNR, BEZEICHNUNG) VALUES (102, 'Vegane Zucchini-Würstchen');
INSERT INTO REZEPT (REZEPTNR, BEZEICHNUNG) VALUES (103, 'Überbackene Kartoffeln');
INSERT INTO REZEPT (REZEPTNR, BEZEICHNUNG) VALUES (104, 'Mozzarella-Salat');
INSERT INTO REZEPT (REZEPTNR, BEZEICHNUNG) VALUES (105, 'Englisches Frühstück');
INSERT INTO REZEPT_ZUTAT (REZEPTNR, ZUTATENNR, MENGE) VALUES (100, 5001, 3);
INSERT INTO REZEPT_ZUTAT (REZEPTNR, ZUTATENNR, MENGE) VALUES (100, 7043, 1);
INSERT INTO REZEPT_ZUTAT (REZEPTNR, ZUTATENNR, MENGE) VALUES (101, 1003, 2);
INSERT INTO REZEPT_ZUTAT (REZEPTNR, ZUTATENNR, MENGE) VALUES (101, 3002, 4);
INSERT INTO REZEPT_ZUTAT (REZEPTNR, ZUTATENNR, MENGE) VALUES (101, 1001, 1);
INSERT INTO REZEPT_ZUTAT (REZEPTNR, ZUTATENNR, MENGE) VALUES (102, 1001, 2);
INSERT INTO REZEPT_ZUTAT (REZEPTNR, ZUTATENNR, MENGE) VALUES (102, 9001, 2);
INSERT INTO REZEPT_ZUTAT (REZEPTNR, ZUTATENNR, MENGE) VALUES (102, 1002, 2);
INSERT INTO REZEPT_ZUTAT (REZEPTNR, ZUTATENNR, MENGE) VALUES (103, 1006, 5);
INSERT INTO REZEPT_ZUTAT (REZEPTNR, ZUTATENNR, MENGE) VALUES (103, 3002, 3);
INSERT INTO REZEPT_ZUTAT (REZEPTNR, ZUTATENNR, MENGE) VALUES (103, 1005, 2);
INSERT INTO REZEPT_ZUTAT (REZEPTNR, ZUTATENNR, MENGE) VALUES (103, 1002, 1);
INSERT INTO REZEPT_ZUTAT (REZEPTNR, ZUTATENNR, MENGE) VALUES (104, 3002, 2);
INSERT INTO REZEPT_ZUTAT (REZEPTNR, ZUTATENNR, MENGE) VALUES (104, 1007, 5);
INSERT INTO REZEPT_ZUTAT (REZEPTNR, ZUTATENNR, MENGE) VALUES (104, 1005, 3);
INSERT INTO REZEPT_ZUTAT (REZEPTNR, ZUTATENNR, MENGE) VALUES (104, 1004, 1);
INSERT INTO REZEPT_ZUTAT (REZEPTNR, ZUTATENNR, MENGE) VALUES (105, 4001, 3);
INSERT INTO REZEPT_ZUTAT (REZEPTNR, ZUTATENNR, MENGE) VALUES (105, 5001, 5);
INSERT INTO REZEPT_ZUTAT (REZEPTNR, ZUTATENNR, MENGE) VALUES (105, 6300, 3);
INSERT INTO REZEPT_ZUTAT (REZEPTNR, ZUTATENNR, MENGE) VALUES (105, 3003, 1);
INSERT INTO REZEPT_ZUTAT (REZEPTNR, ZUTATENNR, MENGE) VALUES (105, 1011, 3);
INSERT INTO REZEPT_ZUTAT (REZEPTNR, ZUTATENNR, MENGE) VALUES (105, 1003, 2);
INSERT INTO REZEPT_ZUTAT (REZEPTNR, ZUTATENNR, MENGE) VALUES (105, 1002, 1);
INSERT INTO REZEPT_KATEGORIE (REZEPTNR, KATEGORIENR) VALUES (100, 5);
INSERT INTO REZEPT_KATEGORIE (REZEPTNR, KATEGORIENR) VALUES (101, 2);
INSERT INTO REZEPT_KATEGORIE (REZEPTNR, KATEGORIENR) VALUES (102, 1);
INSERT INTO REZEPT_KATEGORIE (REZEPTNR, KATEGORIENR) VALUES (103, 2);
INSERT INTO REZEPT_KATEGORIE (REZEPTNR, KATEGORIENR) VALUES (104, 4);
INSERT INTO REZEPT_KATEGORIE (REZEPTNR, KATEGORIENR) VALUES (105, 5);
COMMIT WORK;


